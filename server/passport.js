const passport = require("passport");
const passportJWT = require("passport-jwt");
const User = require('./models/user');
const ExtractJwt = passportJWT.ExtractJwt;
const JwtStrategy = passportJWT.Strategy;

const config = require('./config/passport').authentication;

const jwtOptions = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: config.jwtSecret,
  session: false,
  authScheme: 'Bearer',
  passReqToCallback: true
}
const strategy = new JwtStrategy(jwtOptions, async (req, jwtPayload, next) => {
  // console.log('payload received id: ', jwtPayload._doc._id);
  try {
    const user = await User.findOne({
        _id: jwtPayload._doc._id
    })
    // console.log('User: ', user);
    if (!user) {
      return next(new Error(), false)
    }
    return next(null, user)
  } catch (err) {
    return next(new Error(), false)
  }
});

passport.use(strategy);


module.exports = () => {
  return {
    initialize: function() {
        return passport.initialize();
    }
  };
}