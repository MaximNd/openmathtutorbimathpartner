const router = require('express').Router();
const AuthenticationController = require('./../controllers/AuthenticationController');
const UsersController = require('./../controllers/UsersController');
const isAuthenticated = require('./../policies/isAuthenticated');

router.get('/user/:id', isAuthenticated, UsersController.getUserById);
router.post('/user/login', AuthenticationController.login);
router.post('/user/signup', AuthenticationController.register);

module.exports = router;