const User = require('./../models/user');
const Student = require('./../models/student');
const Teacher = require('./../models/teacher');
const HeadTeacher = require('./../models/headTeacher');
const Director = require('../models/director');
const Authority = require('../models/authority');
const School = require('../models/school');
const fs = require('fs');
const path = require('path');
const uniqid = require('uniqid');
const jwt = require('jsonwebtoken');
const config = require('./../config/passport');

const jwtSignUser = (user) => {
  const ONE_WEEK = 60 * 60 * 24 * 7;
  return jwt.sign(user, config.authentication.jwtSecret, {
    expiresIn: ONE_WEEK
  })
};

function decodeBase64Image(dataString) {
    const matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/);

    if (matches.length !== 3) {
        return new Error('Invalid input string');
    }

    return new Buffer(matches[2], 'base64');
}

function saveImage(imageData, imageName) {
    fs.writeFile(path.join(__dirname, '../public/images', imageName), imageData, err => {
        err ? console.log(err) : console.log('upload success');
    });
}

function getClientByRole(user) {
    const rolesMap = {
        'student': Student,
        'teacher': Teacher,
        'headTeacher': HeadTeacher,
        'director': Director,
        'admin': Authority
    }
    return user.role.map(role => rolesMap[role].find({ userId: user.id }).then(client => client));
}

module.exports = {
    login(req, res) {
        const email = req.body.email;
        const password = req.body.password;
    
        User.findOne({ email: email }, (err, user) => {
            if (err) {
                res.status(500).json({ message: 'Internal Server Error' });
            } else if (user) {
                User.comparePassword(password, user.password, (err, isMatch) => {
                    if (err) {
                        res.status(500).json({ message: 'Internal Server Error' });
                    } else if (isMatch) {
                        user.clients.then(clients => {
                            // console.log();
                            const token = jwtSignUser(user);
                            res.append('access-control-allow-headers', 'Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
                            res.append('access-control-allow-methods', 'GET, POST, PUT, DELETE, OPTIONS');
                            res.append('access-control-expose-headers', 'Authorization');
                            res.append('authorization', `Bearer ${token}`);
                            res.send({
                                user,
                                clients
                            });
                        });
                    } else {
                        res.json({
                            type: false,
                            data: 'Invalid password'
                        });    
                    }
                });
            } else {
                res.json({
                    type: false,
                    data: 'Invalid email'
                });
            }
        });
    },
    register(req, res) {
        console.log('hello');
        const { user, isMainDirector, access } = req.body.director;
        const imageName = uniqid() + path.extname(path.basename(user.image.name));
    
        saveImage(decodeBase64Image(user.image.data), imageName);
        user.image = imageName;
    
        // create user
        User.createUser(new User(user), (err, userId) => {
            if (err) {
                console.log(err);
            }
            
            // create director
            const director = new Director({
                userId,
                isMainDirector,
                access
            });
            
            director.save(err => {
                if (err) {
                    console.log(err);
                }
    
                if (typeof req.body.authority !== 'undefined') {
                    const { user, company } = req.body.authority;
                    const imageName = uniqid() + path.extname(path.basename(user.image.name));
                    
                    saveImage(decodeBase64Image(user.image.data), imageName);
                    user.image = imageName;
                    
                    User.createUser(new User(user), (err, userId) => {
                        if (err) {
                            console.log(err);
                        }
                
                        // create authority
                        const authority = new Authority({
                            userId,
                            company
                        });
                        authority.save(err => {
                            if (err) {
                                console.log(err);
                            }

                            // create school
                            const school = new School({
                                name: req.body.school.name,
                                number: req.body.school.number,
                                isPhilial: req.body.school.isPhilial,
                                country: req.body.school.country,
                                region: req.body.school.region,
                                city: req.body.school.city,
                                district: req.body.school.district,
                                address: req.body.school.address,
                                authorityId: authority._id,
                                directorId: director._id
                            });
                            
    
                            school.save(err => {
                                if (err) {
                                    console.log(err);
                                }
                        
                                // search director and add school id
                                Director.findById({ _id: director._id }, (err, doc) => {
                                    if (err) {
                                        console.log(err);
                                    }
                        
                                    doc.schoolId = school._id;
                                    doc.save(err => {
                                        if (err) {
                                            console.log(err);
                                        }
    
                                        res.status(200, { message: 'ok' }).end();
                                    });
                                });
                            });
                        });
                    });
                } else {
                    // create school
                    const school = new School({
                        name: req.body.school.name,
                        number: req.body.school.number,
                        isPhilial: req.body.school.isPhilial,
                        country: req.body.school.country,
                        region: req.body.school.region,
                        city: req.body.school.city,
                        district: req.body.school.district,
                        address: req.body.school.address,
                        authorityId: req.body.school.authorityId,
                        directorId: director._id
                    });
    
                    school.save(err => {
                        if (err) {
                            console.log(err);
                        }
                
                        // search director and add school id
                        Director.findById({ _id: director._id }, (err, doc) => {
                            if (err) {
                                console.log(err);
                            }
                
                            doc.schoolId = school._id;
                            doc.save(err => {
                                if (err) {
                                    console.log(err);
                                }
    
                                res.status(200, { message: 'ok' }).end();
                            });
                        });
                    });
                }
            });
        });
    }
};